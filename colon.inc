%macro colon 2
%2:
%ifndef prev
	%define prev 0
%endif
dq prev
db %1, 0
%define prev %2
%endmacro
