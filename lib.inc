section .rodata
newline_char:
	db 	10
space_char:
	db	32
tab_char:
	db	9
minus_char:
	db 	45

section .text
global exit
global string_length
global print_err_string
global print_err_newline
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global pars_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
	mov	rax, 60
	syscall
	ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor	rax, rax
.loop:	cmp	byte[rdi+rax], 0
	je	.end
	inc	rax
	jmp	.loop
.end:	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_err_string:
	call	string_length
	mov	rsi, rdi
	mov	rdx, rax
	mov	rax, 1
	mov	rdi, 2
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA в stderr)
print_err_newline:
	mov	rdi, [newline_char]
	push	rdi
	mov	rax, 1
	mov	rsi, rsp
	mov	rdi, 2
	mov	rdx, 1
	syscall
	pop	rdi
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call	string_length
	mov	rsi, rdi
	mov	rdx, rax
	mov	rax, 1
	mov	rdi, 1
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov	rdi, [newline_char]
; Принимает код символа и выводит его в stdout
print_char:
	push	rdi
	mov	rax, 1
	mov	rsi, rsp
	mov	rdi, 1
	mov	rdx, 1
	syscall
	pop	rdi
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push	r12
	push	rsp
	mov	r12, rsp
	sub	rsp, 21
	mov	r9, rsp	
	add	r9, 20
	mov	rax, rdi
	
	mov	byte[r9], 0
	dec	r9

.loop1: xor	rdx, rdx
	mov	r8, 10
	div	r8 ; quoitent -> rax, remainder -> rdx
	mov	rdi, 0x30
	add	dil, dl
	mov	byte[r9], dil
	dec	r9

	test	rax, rax
	jz	.out
	jmp	.loop1

.out:	inc	r9
	mov	rdi, r9
	call	print_string

.end:	add	rsp, 21
	pop	rsp
	pop	r12
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp 	rdi, 0
	js	.neg
	call	print_uint
	jmp	.end
.neg:	push	rdi
	mov	rdi, [minus_char]
	call	print_char
	pop	rdi
	dec	rdi
	xor	rdi, 0xFFFFFFFFFFFFFFFF
	call	print_uint
.end:	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push	r12
	push	r13
	push	r14
	push	r15

	call	string_length
	mov	r12, rax 	; r12 contains length of the first string

	push	rdi
	mov	rdi, rsi
	call	string_length
	pop	rdi
	mov	r13, rax 	; r13 contains length of the second string
	cmp	r12, r13
	jne	.NonEq
	xor	r14, r14	; counter
	xor	rax, rax
	mov	rax, 1

.loop:	cmp	r14, r12
	je	.end
	mov	r15b, byte[rdi + r14]
	cmp	r15b, byte[rsi + r14]
	jne	.NonEq
	inc	r14
	jmp	.loop

.NonEq: xor	rax, rax 	; 1 -> rax if equal, otherwise 0 -> rax

.end:	pop	r15	
	pop	r14
	pop	r13
	pop	r12
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push	0
	xor	rax, rax
	xor	rdi, rdi
	mov	rsi, rsp
	mov	rdx, 1
	syscall
.end:	pop	rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	; rdi contains buffer pointer
	; rsi contains buffer length
	push	r12
	push	r14
	push	r15
	
 	mov	r14, rdi
 	mov	r15, rsi
	xor	r12, r12
	
.skip:	call	read_char
	cmp	al, [newline_char]
	je	.skip
	cmp	al, [tab_char]
	je	.skip
	cmp	al, [space_char]
	je	.skip
	cmp	al, 0
	je	.null
 
 	mov	byte[r14+r12], al
 	inc	r12

.read:	cmp	r12, r15
	je	.fail
	call	read_char
;	cmp	al, [space_char]
;	je	.null
	cmp	al, [newline_char]
	je	.null
	cmp	al, [tab_char]
	je	.null
	cmp	al, 0
	je	.null
	mov	byte[r14+r12], al
	inc	r12
	jmp	.read
 
.fail:	xor	rax, rax
	xor	rdx, rdx
	jmp	.end
 
.null:	mov	byte[r14+r12], 0
	mov	rax, r14
	mov	rdx, r12
 
.end:	pop	r15
	pop	r14
	pop	r12
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	; rdi contains string pointer
	push	r12
	push	r13
	push	r14
	push	r15
	mov	r12, rdi
	call	string_length
	mov	r13, rax
	xor	r14, r14
	xor	rax, rax
.loop:	cmp	r13, r14
	je	.ret
	cmp	byte[r12+r14], 48
	jl	.ret
	cmp	byte[r12+r14], 57
	jg	.ret

	xor	r15, r15
	mov	r15b, 10
	mul	r15
	mov	r15b, byte[r12+r14]
	sub	r15b, '0'
	add	rax, r15
	inc	r14
	jmp	.loop
	
.ret:	mov	rdx, r14
	cmp	rdx, 0
	je	.end
	cmp	rax, 0
	je	.zer
	jmp	.end
.zer:	mov	rdx, 1
.end:	pop	r15
	pop	r14
	pop	r13
	pop	r12
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	push	r12
	mov	r12b, byte[rdi]
	cmp	r12b, [minus_char]
	je	.neg
	call	parse_uint
	jmp	.end
.neg:	inc	rdi
	call	parse_uint
	cmp	rdx, 0
	je	.end
	cmp	rax, 0
	je	.zer
	neg	rax
	inc	rdx
	jmp	.end
.zer:	mov	rdx, 1

.end:	pop	r12
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	; rdi contains	string pointer
	; rsi contains	buffer pointer
	; rdx contains	buffer length
	push	r14

	test	rdx, rdx
	jz	.err

	push	rdi
	push	rsi
	push	rdx
	call	string_length
	pop	rdx
	pop	rsi
	pop	rdi
	inc	rax
	cmp	rax, rdx
	jg	.err

	dec	rax
	xor	r14, r14

.loop:	cmp	r14, rax
	jg	.end
	mov	r15b, byte[rdi + r14]
	mov	byte[rsi + r14], r15b
	inc	r14
	jmp	.loop
	
.err:	xor	rax, rax

.end:	pop	r14
	ret

