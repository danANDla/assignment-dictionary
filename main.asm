section .text
%include "colon.inc"
%include "lib.inc"

section .rodata
%include "words.inc"
err_read_word:
db 	"failed to read a key", 0
err_find_word:
db 	"failed to find a key", 0

section .text
extern	find_word

global _start
_start:
sub	rsp, 256
mov	rdi, rsp
mov	rsi, 256
call	read_word
test	rax, rax
jz	.err_read
mov	rdi, rax
mov	rsi, prev
call	find_word
test    rax, rax
jz      .err_find
add     rax, 8
push    rax
mov     rdi, rax
call    string_length
pop     rdi
inc     rdi
add     rdi, rax
call    print_string
call    print_newline
jmp	.end

.err_read:
mov	rdi, err_read_word
call	print_err_string
call	print_err_newline
jmp	.end

.err_find:
mov	rdi, err_find_word
call	print_err_string
call	print_err_newline

.end:
add	rsp, 256
call	exit

