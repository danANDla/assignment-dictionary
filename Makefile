ASM=nasm
FLAGS=-felf64

main: main.o dict.o
	ld -o $@ $^
main.o: main.asm words.inc colon.inc lib.inc
	$(ASM) $(FLAGS) -o $@ $<
%.o: %.asm
	$(ASM) $(FLAGS) -o $@ $<
clean:
	rm -f *.o
