section .text
extern string_equals
global find_word

; return addres of begenning of the entry in the dictionary
; rdi --- pointer on null-termainated string (key to find)
; rsi --- pointer on beginning of dictionary
find_word:
.loop:
test    rsi, rsi
jz	.unknown
push	rdi
push	rsi
add	rsi, 8 ; now it points to the key for check
call	string_equals
pop	rsi
pop	rdi
cmp	rax, 1
je	.thats
mov	rsi, [rsi]
jmp	.loop
.unknown:
xor	rax, rax
ret
.thats:
mov	rax, rsi
ret
